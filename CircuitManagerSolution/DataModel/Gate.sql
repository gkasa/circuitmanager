﻿CREATE TABLE [dbo].[Gate]
(
    [Id] UNIQUEIDENTIFIER NOT NULL DEFAULT newid(), 
    [GateId] UNIQUEIDENTIFIER NOT NULL DEFAULT newsequentialid(), 
    [OutputPinId] UNIQUEIDENTIFIER NOT NULL, 
    [Label] NVARCHAR(50) NULL, 
    CONSTRAINT [PK_Gate] PRIMARY KEY NONCLUSTERED ([Id]), 
    CONSTRAINT [UQ_Gate_GateId] UNIQUE ([GateId]), 
    CONSTRAINT [FK_Gate_OutputPinId_Pin_PinId] FOREIGN KEY ([OutputPinId]) REFERENCES [Pin]([PinId]) 
)
