﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructures
{

    /// <summary>
    /// The possible values a Pin can have.
    /// </summary>
    public enum LogicalValue
    {
        Unknown,
        Low,
        High
    }

    public struct PinValue : IEquatable<PinValue>
    {
        private LogicalValue _value;

        public LogicalValue Value
        {
            get
            {
                return _value;
            }
        }

        public PinValue(LogicalValue value)
        {
            _value = value;
        }

        public PinValue(bool? value)
        {
            if (value == null)
            {
                _value = LogicalValue.Unknown;
            }
            else if (value == true)
            {
                _value = LogicalValue.High;
            }
            else 
            {
                _value = LogicalValue.Low;
            }
        }

        public override bool Equals(object obj)
        {
            return obj != null
                && obj is PinValue
                && Equals((PinValue)obj);
        }

        public bool Equals(PinValue obj)
        {
            return obj.Value == Value;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{0}", Value);
        }

        public static bool operator <(PinValue x, PinValue y)
        {
            return x.Value < y.Value;
        }

        public static bool operator <=(PinValue x, PinValue y)
        {
            return x.Value <= y.Value;
        }

        public static bool operator >(PinValue x, PinValue y)
        {
            return x.Value > y.Value;
        }

        public static bool operator >=(PinValue x, PinValue y)
        {
            return x.Value >= y.Value;
        }

        public static bool operator ==(PinValue x, PinValue y)
        {
            return x.Value == y.Value;
        }

        public static bool operator !=(PinValue x, PinValue y)
        {
            return x.Value != y.Value;
        }

        public static bool operator true(PinValue val)
        {
            return val.Value == LogicalValue.High;
        }

        public static bool operator false(PinValue val)
        {
            return val.Value == LogicalValue.Low;
        }

        public static PinValue operator &(PinValue x, PinValue y)
        {
            if (x.Value == LogicalValue.High 
                && y.Value == LogicalValue.High)
            {
                return new PinValue(LogicalValue.High);
            }
            else if (x.Value == LogicalValue.Low 
                     || y.Value == LogicalValue.Low)
            {
                return new PinValue(LogicalValue.Low);
            }

            return new PinValue(LogicalValue.Unknown);
        }

        public static PinValue operator |(PinValue x, PinValue y)
        {
            if (x.Value == LogicalValue.High
                || y.Value == LogicalValue.High)
            {
                return new PinValue(LogicalValue.High);
            }
            else if(x.Value == LogicalValue.Low
                    && y.Value == LogicalValue.Low)
            {
                return new PinValue(LogicalValue.Low);
            }

            return new PinValue(LogicalValue.Unknown);
        }

        public static PinValue operator ^(PinValue x, PinValue y)
        {
            if (x.Value != LogicalValue.Unknown
                && y.Value != LogicalValue.Unknown)
            {
                if (x.Value != y.Value)
                {
                    return new PinValue(LogicalValue.High);
                }

                return new PinValue(LogicalValue.Low);
            }

            return new PinValue(LogicalValue.Unknown);
        }

        public static PinValue operator !(PinValue x)
        {
            PinValue result;

            if (x.Value == LogicalValue.Low)
            {
                result = new PinValue(true);
            }
            else if (x.Value == LogicalValue.High)
            {
                result = new PinValue(false);
            }
            else
            {
                result = new PinValue(null);
            }

            return result;
        }

        public static implicit operator PinValue(bool? val)
        {
            return new PinValue(val);
        }

        public static implicit operator bool(PinValue val)
        {
            return val.Value == LogicalValue.High;
        }

        public static implicit operator bool?(PinValue val)
        {
            if (val.Value == LogicalValue.High)
            {
                return true;
            }
            else if (val.Value == LogicalValue.Low)
            {
                return false;
            }

            return null;
        }
    }
}
