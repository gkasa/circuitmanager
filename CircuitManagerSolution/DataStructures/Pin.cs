﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace DataStructures
{
    /// <summary>
    /// A pin used in the various logical gates.
    /// </summary>
    public class Pin : Observable<Pin>, IEquatable<Pin>
    {
        // variables
        private PinValue _value;
        private string _label;
        private readonly Guid _id = Guid.NewGuid();
        private readonly ICollection<PinConnection> _connections
            = new List<PinConnection>();

        // properties
        /// <summary>
        /// Get the ID of the Pin.
        /// </summary>
        public Guid Id
        {
            get { return _id; }
        }

        /// <summary>
        /// Get or Set the value of the Pin.
        /// </summary>
        public PinValue Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
                Notify(this);
            }
        }

        /// <summary>
        /// Get or Set the label of the Pin.
        /// </summary>
        public string Label
        {
            get
            {
                return _label;
            }
            set
            {
                _label = value;
                Notify(this);
            }
        }

        /// <summary>
        /// Get or Set the Gate containing this Pin.
        /// </summary>
        public Gate ContainingGate { get; set; }

        /// <summary>
        /// Get the connections of this Pin.
        /// </summary>
        public ICollection<PinConnection> Connections
        {
            get
            {
                return _connections;
            }
        }

        /// <summary>
        /// Get the pins this Pin is connected to.
        /// </summary>
        public IEnumerable<Pin> ConnectedTo
        {
            get
            {
                return Connections.Where(c => c.From != this)
                                  .Select(c => c.From)
                                  .Union(Connections.Where(c => c.To != this)
                                                    .Select(c => c.To));
            }
        }

        // constructors
        /// <summary>
        /// The default constructor of a Pin.
        /// </summary>
        public Pin()
            : this(new PinValue(LogicalValue.Unknown), label: null)
        {

        }

        /// <summary>
        /// A constructor of a Pin.
        /// </summary>
        /// <param name="code">
        /// The initial code of the new Pin.
        /// </param>
        public Pin(string code)
            : this(new PinValue(LogicalValue.Unknown), label: null)
        {
            
        }

        /// <summary>
        /// A constructor of a Pin.
        /// </summary>
        /// <param name="value">
        /// The initial value of the new Pin.
        /// </param>
        /// <param name="code">
        /// The initial code of the new Pin.
        /// </param>
        public Pin(PinValue value)
            : this(value, label: null)
        {
            
        }

        /// <summary>
        /// A constructor of a Pin.
        /// </summary>
        /// <param name="value">
        /// The initial value of the new Pin.
        /// </param>
        /// <param name="code">
        /// The initial code of the new Pin.
        /// </param>
        /// <param name="label">
        /// The initial label of the new Pin.
        /// </param>
        public Pin(PinValue value, string label)
        {
            Value = value;
            Label = label;
        }
        
        // methods
        /// <summary>
        /// A method that builds and returns a string
        /// representation of a Pin.
        /// </summary>
        /// <returns>
        /// A string representation of a Pin.
        /// </returns>
        public override string ToString()
        {
            return string.Format(
                "[Id: {0} | Label: {1} | Value: {2}]",
                Id,
                Label,
                Value);
        }

        /// <summary>
        /// A method that builds and returns the
        /// hash code of a Pin.
        /// </summary>
        /// <returns>
        /// The hash code of a Pin.
        /// </returns>
        public override int GetHashCode()
        {
            // Id is the only immutable value of this object
            return Id.GetHashCode();
        }

        /// <summary>
        /// A method that checks if this Pin is
        /// equal to another object.
        /// </summary>
        /// <param name="obj">
        /// The object to compare for equality with
        /// this Pin.
        /// </param>
        /// <returns>
        /// TRUE if this Pin and the specified object 
        /// are equal, FALSE otherwise.
        /// </returns>
        public override bool Equals(object obj)
        {
            return obj != null 
                && (obj is Pin) 
                && Equals(obj as Pin);
        }

        /// <summary>
        /// A method that checks if this Pin is
        /// equal to another Pin.
        /// </summary>
        /// <param name="otherPin">
        /// The Pin to compare for equality with
        /// this Pin.
        /// </param>
        /// <returns>
        /// TRUE if this Pin and the specified other 
        /// Pin are equal, FALSE otherwise.
        /// </returns>
        public bool Equals(Pin otherPin)
        {
            if (otherPin == null)
            {
                return false;
            }

            // fail fast, Id is more probable to be different
            return Id == otherPin.Id
                // check only if Ids are equal
                && Value == otherPin.Value
                && Label == otherPin.Label;
        }

        public PinConnection ConnectTo(Pin destination)
        {
            return new PinConnection(from: this, 
                                     to: destination);
        }
    }
}
