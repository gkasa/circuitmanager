﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructures
{
    public abstract class Gate : IObserver<Pin>, ILogicComponent
    {
        // fields
        private readonly Guid _id = Guid.NewGuid();
        private Pin _output;
        private IDisposable _outputUnsubscriber;

        // properties
        public Guid Id
        {
            get
            {
                return _id;
            }
        }

        public string Code { get; set; }

        public string Label { get; set; }

        public Pin Output
        {
            get
            {
                return _output;
            }
            protected set
            {
                _output = value;

                if (_outputUnsubscriber != null)
                {
                    _outputUnsubscriber.Dispose();
                }

                if (_output != null)
                {
                    _output.ContainingGate = this;
                    _outputUnsubscriber = _output.Subscribe(this);
                    _output.Notify(_output);
                }
            }
        }

        public abstract IEnumerable<PinValue> InputValues { get; }

        public IEnumerable<PinValue> OutputValues
        {
            get
            {
                //PinValue[] result = new PinValue[0];

                if (Output != null)
                {
                    //result = new PinValue[] { Output.Value };
                    yield return Output.Value;
                }

                //return result;
            }
        }
        
        // methods
        public abstract void OnCompleted();

        public abstract void OnError(Exception error);

        public abstract void OnNext(Pin value);
    }
}
