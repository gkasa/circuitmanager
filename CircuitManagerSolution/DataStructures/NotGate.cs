﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructures
{
    public class NotGate : UnaryGate
    {
        public NotGate(bool? input)
        {
            Output = new Pin();
            Input = new Pin(input);
        }

        public NotGate() : this(null) { }

        /// <summary>
        /// Not implemented. Don't use.
        /// </summary>
        public override void OnCompleted()
        {
            
        }

        public override void OnError(Exception error)
        {
            throw error;
        }

        public override void OnNext(Pin value)
        {
            if (Input != null 
                && Output != null
                && Output.Value != (!Input.Value))
            {
                Output.Value = !Input.Value;
            }
        }
    }
}
