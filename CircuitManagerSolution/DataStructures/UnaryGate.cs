﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructures
{
    public abstract class UnaryGate : Gate
    {
        // variables
        private Pin _input;
        private IDisposable _inputUnsubscriber;

        // properties
        /// <summary>
        /// Get or Set the input pin of the gate.
        /// </summary>
        /// <remarks>
        /// The Set operation is done only if the 
        /// new value is different from the current.
        /// </remarks>
        public Pin Input
        {
            get
            {
                return _input;
            }
            protected set
            {
                _input = value;

                if (_inputUnsubscriber != null)
                {
                    _inputUnsubscriber.Dispose();
                }

                if (_input != null)
                {
                    _input.ContainingGate = this;
                    _inputUnsubscriber = _input.Subscribe(this);
                    _input.Notify(_input);
                }
            }
        }

        public override IEnumerable<PinValue> InputValues
        {
            get
            {
                if (Input != null)
                {
                    yield return Input.Value;
                }
            }
        }
    }
}
