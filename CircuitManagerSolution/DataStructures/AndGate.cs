﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructures
{
    public class AndGate : BinaryGate
    {
        // constructors
        
        public AndGate(bool? input1, bool? input2)
        {
            Output = new Pin();
            Input1 = new Pin(input1);
            Input2 = new Pin(input2);
        }

        public AndGate()
            : this(null, null)
        { }

        // methods

        /// <summary>
        /// Not implemented. Don't use.
        /// </summary>
        public override void OnCompleted()
        {
            
        }

        public override void OnError(Exception error)
        {
            throw error;
        }

        public override void OnNext(Pin value)
        {
            if (Output != null 
                && Input1 != null 
                && Input2 != null
                && Output.Value != (Input1.Value && Input2.Value))
            {
                Output.Value = (Input1.Value && Input2.Value);
            }
        }
    }
}
