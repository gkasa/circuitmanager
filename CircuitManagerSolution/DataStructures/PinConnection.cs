﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructures
{
    /// <summary>
    /// A class that simulates a connection between two pins.
    /// </summary>
    /// <remarks>
    /// The connection is directional, it expects a source pin
    /// and a destination pin. The value propagates from the 
    /// source pin to the destination.
    /// </remarks>
    public class PinConnection : IObserver<Pin>
    {
        // fields
        private Pin _from;
        private Pin _to;
        private IDisposable _fromUnsubscriber;
        private IDisposable _toUnsubscriber;

        // properties

        /// <summary>
        /// Get or Set the label of this PinConnection object.
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Get or Set the source pin.
        /// </summary>
        public Pin From
        {
            get
            {
                return _from;
            }
            set
            {
                //if (value.Value == _from.Value)
                //{
                //    return;
                //}

                _from = value;

                if (_fromUnsubscriber != null)
                {
                    _fromUnsubscriber.Dispose();
                }

                if (_from != null)
                {
                    _from.Connections.Add(this);
                    _fromUnsubscriber = _from.Subscribe(this);
                    _from.Notify(_from);
                }
            }
        }

        /// <summary>
        /// Get or Set the destination pin.
        /// </summary>
        public Pin To
        {
            get
            {
                return _to;
            }
            set
            {
                //if (value.Value == _to.Value)
                //{
                //    return;
                //}

                _to = value;

                if (_toUnsubscriber != null)
                {
                    _toUnsubscriber.Dispose();
                }

                if (_to != null)
                {
                    _to.Connections.Add(this);
                    _toUnsubscriber = _to.Subscribe(this);
                    _to.Notify(_to);
                }
            }
        }
        
        // constructors
        public PinConnection()
            : this(null, null)
        {
            
        }

        public PinConnection(Pin from)
            : this(from, null)
        {
            
        }

        public PinConnection(Pin from, Pin to)
        {
            To = to;
            From = from;
        }

        /// <summary>
        /// Not yet implemented. Don't use.
        /// </summary>
        public void OnCompleted()
        {
            
        }

        public void OnError(Exception error)
        {
            throw error;
        }

        public void OnNext(Pin value)
        {
            if (From != null
                && To != null
                && From.Value != To.Value)
            {
                To.Value = From.Value;
            }
        }
    }
}
