﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructures
{
    public class NandGate : BinaryGate
    {
        private AndGate _andGate;
        private NotGate _notGate;
        private PinConnection _input1Connection;
        private PinConnection _input2Connection;
        private PinConnection _andToNotConnection;
        private PinConnection _outputConnection;

        public NandGate(bool? input1, bool? input2)
        {
            _andGate = new AndGate(input1, input2);
            _notGate = new NotGate();
            Output = new Pin();
            Input1 = new Pin(input1);
            Input2 = new Pin(input2);

            _input1Connection = new PinConnection(
                from: Input1, 
                to: _andGate.Input1);
            _input2Connection = new PinConnection(
                from: Input2, 
                to: _andGate.Input2);
            _andToNotConnection = new PinConnection(
                from: _andGate.Output, 
                to: _notGate.Input);
            _outputConnection = new PinConnection(
                from: _notGate.Output, 
                to: Output);
        }

        public NandGate() : this(null, null) { }

        /// <summary>
        /// Not implemented. Don't use.
        /// </summary>
        public override void OnCompleted()
        {
            
        }

        public override void OnError(Exception error)
        {
            throw error;
        }

        public override void OnNext(Pin value)
        {
            // It shouldn't be necessary to implement this method
            // as it's already implemented in the inner gates'
            // implementation.
        }
    }
}
