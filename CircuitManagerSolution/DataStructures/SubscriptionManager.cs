﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructures
{
    public class SubscriptionManager<T> : IDisposable
    {
        private ICollection<IObserver<T>> _observers;
        private IObserver<T> _observer;

        public SubscriptionManager(ICollection<IObserver<T>> observers, IObserver<T> observer)
        {
            if (observers == null)
            {
                throw new ArgumentNullException("observers");
            }

            _observers = observers;
            _observer = observer;
        }

        //public void Subscribe(IObserver<T> observer)
        //{
        //    _observers.Add(observer);
        //}

        //public void Notify(T obj)
        //{
        //    foreach (var observer in _observers)
        //    {
        //        observer.OnNext(obj);
        //    }
        //}

        public void Dispose()
        {
            _observers.Remove(_observer);
        }
    }
}
