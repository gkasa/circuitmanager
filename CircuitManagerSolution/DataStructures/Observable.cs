﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructures
{
    public class Observable<T> : IObservable<T>
    {
        private ICollection<IObserver<T>> _observers;

        public Observable()
        {
            _observers = new List<IObserver<T>>();
        }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            if (!_observers.Contains(observer))
            {
                _observers.Add(observer);
            }

            return new SubscriptionManager<T>(_observers, observer);
        }

        public void Notify(T obj)
        {
            foreach (var observer in _observers)
            {
                observer.OnNext(obj);
            }
        }
    }
}
