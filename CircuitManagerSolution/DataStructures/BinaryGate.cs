﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructures
{
    public abstract class BinaryGate : Gate
    {
        // variables
        private Pin _input1;
        private Pin _input2;
        private IDisposable _input1Unsubscriber;
        private IDisposable _input2Unsubscriber;

        // properties
        /// <summary>
        /// Get the first input pin of this binary gate.
        /// </summary>
        public Pin Input1
        {
            get
            {
                return _input1;
            }
            protected set
            {
                _input1 = value;

                if (_input1Unsubscriber != null)
                {
                    _input1Unsubscriber.Dispose();
                }

                if (_input1 != null)
                {
                    _input1.ContainingGate = this;
                    _input1Unsubscriber = _input1.Subscribe(this);
                    _input1.Notify(_input1);
                }
            }
        }

        /// <summary>
        /// Get the second input pin of this binary gate.
        /// </summary>
        public Pin Input2
        {
            get
            {
                return _input2;
            }
            protected set
            {
                _input2 = value;

                if (_input2Unsubscriber != null)
                {
                    _input2Unsubscriber.Dispose();
                }

                if (_input2 != null)
                {
                    _input2.ContainingGate = this;
                    _input2Unsubscriber = _input2.Subscribe(this);
                    _input2.Notify(_input2);
                }
            }
        }

        /// <summary>
        /// Get the input values for this binary gate.
        /// </summary>
        public override IEnumerable<PinValue> InputValues
        {
            get
            {
                if (Input1 != null)
                {
                    yield return Input1.Value;
                }

                if (Input2 != null)
                {
                    yield return Input2.Value;
                }
            }
        }
    }
}
