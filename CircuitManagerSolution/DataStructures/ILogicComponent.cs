﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructures
{
    /// <summary>
    /// Everything that has some input data (in the form of Pins), 
    /// some calculation of some sort, and some output data is 
    /// considered as an ILogicComponent.
    /// </summary>
    public interface ILogicComponent
    {
        IEnumerable<PinValue> InputValues { get; }
        IEnumerable<PinValue> OutputValues { get; }
    }
}
