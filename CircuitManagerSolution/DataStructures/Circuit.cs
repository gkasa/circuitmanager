﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructures
{
    public class Circuit : ILogicComponent, IEnumerable<Gate>
    {
        private readonly IEnumerable<Pin> _inputPins;
        private readonly IEnumerable<Pin> _outputPins;
        private IEnumerable<ILogicComponent> _components;

        public IEnumerable<PinValue> InputValues
        {
            get
            {
                foreach (var pin in _inputPins)
                {
                    yield return pin.Value;
                }
            }
        }

        public IEnumerable<PinValue> OutputValues
        {
            get
            {
                foreach (var pin in _outputPins)
                {
                    yield return pin.Value;
                }
            }
        }

        public IEnumerable<Gate> Gates
        {
            get
            {
                foreach (var component in _components)
                {
                    if (component is Gate)
                    {
                        yield return component as Gate;
                    }
                }
            }
        }

        public IEnumerable<UnaryGate> UnaryGates
        {
            get
            {
                foreach (var component in _components)
                {
                    if (component is UnaryGate)
                    {
                        yield return component as UnaryGate;
                    }
                }
            }
        }

        public IEnumerable<BinaryGate> BinaryGates
        {
            get
            {
                foreach (var component in _components)
                {
                    if (component is BinaryGate)
                    {
                        yield return component as BinaryGate;
                    }
                }
            }
        }

        public Circuit(IEnumerable<PinValue> inputValues)
        {
            _inputPins = inputValues.Select(pinValue => new Pin(pinValue));
        }

        public Circuit(IEnumerable<bool?> inputValues)
        {
            _inputPins = inputValues.Select(value => new Pin(value));
        }

        public IEnumerator<Gate> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
