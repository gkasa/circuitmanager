﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataStructures;

namespace CircuitManagerUnitTest.DataStructures
{
    [TestClass]
    public class PinConnectionUnitTest
    {
        [TestMethod]
        public void DistributionOfValuesTest()
        {
            Pin from = new Pin();
            Pin to = new Pin();
            PinConnection connection = new PinConnection(from, to);

            Assert.AreEqual(from.Value, to.Value);
            Assert.AreEqual(to.Value, from.Value);
        }
    }
}
