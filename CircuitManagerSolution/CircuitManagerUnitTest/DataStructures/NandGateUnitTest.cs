﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataStructures;

namespace CircuitManagerUnitTest.DataStructures
{
    [TestClass]
    public class NandGateUnitTest
    {
        private NandGate gate1 = new NandGate();
        private NandGate gate2 = new NandGate(true, false);
        private NandGate gate3 = new NandGate(false, false);
        private NandGate gate4 = new NandGate(true, true);
        private NandGate gate5 = new NandGate(null, true);
        private NandGate gate6 = new NandGate(null, false);

        [TestMethod]
        public void ValueTest()
        {
            Assert.AreEqual(LogicalValue.Unknown, gate1.Output.Value.Value);
            Assert.AreEqual(LogicalValue.High, gate2.Output.Value.Value);
            Assert.AreEqual(LogicalValue.High, gate3.Output.Value.Value);
            Assert.AreEqual(LogicalValue.Low, gate4.Output.Value.Value);
            Assert.AreEqual(LogicalValue.Unknown, gate5.Output.Value.Value);
            Assert.AreEqual(LogicalValue.High, gate6.Output.Value.Value);
        }
    }
}
