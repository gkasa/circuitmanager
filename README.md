# README #

### What is this repository for? ###

* Quick summary

This software is meant to become a circuit designer and simulator. The final result will be a web application backed by a database that will allow users to create, store, and modify circuit schemas.

* Version
0.0.1

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

Just download the VS solution and run it in VS. Actually developed using VS 2013.

* Configuration

* Dependencies

None.

* Database configuration

None, for now.

* How to run tests

* Deployment instructions
None yet.

### Contribution guidelines ###

* Writing tests

Write tests in the UnitTest projects inside the solution.

* Code review

* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

Gentian Kasa: kasa.gentian@gmail.com

* Other community or team contact